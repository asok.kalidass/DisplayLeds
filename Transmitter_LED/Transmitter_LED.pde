//namespaces
import processing.serial.*;
import java.io.*;
//Declarations
Serial port;
int count = 0;
String [] drumData;
boolean isRead = true;

void setup(){
 
 //Open the serial port for communication with the Arduino
 //Make sure the COM port is correct
 port = new Serial(this, "COM3", 9600);
 port.bufferUntil('\n');

}

void draw() {
    
 if (isRead) {   
 //Read file from path
 readTextFile("C:/toolbox/SensorData.txt");
 
 //Read the entire file contents
 isRead = false;
 }
 //read each bit from the text file
 if (count < drumData.length) {

 port.write(drumData[count]);
 delay(700);
 //after displaying the required color turn off the led so that next state can be shown.
 //hence, 0 - denoting no color, is sent 
 port.write('0');
 delay(100);
 count++;
 } 
 //to denote all the bites were read
 else {
 delay(10000);
 isRead = true;
 }
} 


//Read the text file 
void readTextFile(String sensorData){
 
 File file = new File(sensorData);
 BufferedReader reader = null;
 StringBuilder contents = new StringBuilder();
 try{
 reader = new BufferedReader(new FileReader(file));
 String data = null;
 //read file 
 while((data = reader.readLine()) != null){
   if (!(data.isEmpty())) {          
        contents.append(data);
        contents.append("\n");
        }
 }
  //read each bits of drum data
 drumData = contents.toString().split(",");
 saveStrings("C:/toolbox/testing.txt", drumData);
 }catch(IOException e){
 e.printStackTrace();
 }finally{
 try {
 if (reader != null){
 reader.close();
 }
 } catch (IOException e) {
 e.printStackTrace();
 }
 }
}