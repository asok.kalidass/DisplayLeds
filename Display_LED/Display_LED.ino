/*
 * LED display system based on the bytes transmitted over com port. 
 */
void setup() { 
 //set the digital read pins
 pinMode(6, OUTPUT);
 pinMode(7, OUTPUT);
 pinMode(8, OUTPUT);
//initiate the listerner 
 Serial.begin(9600);
}

void loop() {
 //declarations
 byte byteRead;

 //check for serial port availability
 if (Serial.available()) {
 
 //read from serial port 
 byteRead = Serial.read();

//read the bytes 
byteRead = byteRead - '0';

 if (byteRead > 0){
 digitalWrite((byteRead), HIGH); //turn on the LED
 }
 //turn off the LEDs
 else if ((byteRead == 0)) {
 digitalWrite(6, LOW);
 digitalWrite(7, LOW);
 digitalWrite(8, LOW);
 }
 }
 }
